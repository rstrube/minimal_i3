#!/bin/bash

# kill any running instances of compton
killall -q compton

# restore wallpaper
nitrogen --restore

sleep 1

# launch compton
$HOME/.config/compton/launch.sh

sleep 1

# launch polybar
$HOME/.config/polybar/launch.sh
