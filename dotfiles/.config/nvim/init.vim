set mouse=a			" support mouse functionality
set number			" show line numbers
syntax on
colorscheme sublimemonokai	" set color scheme
set t_Co=256			" vim-monokai now only support 256 colours in terminal.

" ctrl+c yanks to the + register (in X this is CLIPBOARD)
vnoremap <C-c> "+y

" ctrl+v pastes from the + register.
" note: ctrl+o switches back to NORMAL mode for one command, in this case pasting from the + register
inoremap <C-v> <C-o>"+p

" switch back to normal mode easily
inoremap jk <ESC>

" set the timout to be a little tighter
set timeout timeoutlen=500
