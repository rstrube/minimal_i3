#!/bin/sh

mkdir -p $HOME/.config/i3
mkdir -p $HOME/.config/i3blocks
mkdir -p $HOME/.config/polybar
mkdir -p $HOME/.config/compton
mkdir -p $HOME/.config/rofi
mkdir -p $HOME/.config/nvim/colors
mkdir -p $HOME/.config/gtk-3.0
mkdir -p $HOME/.config/nitrogen
mkdir -p $HOME/.config/Code\ -\ OSS/User
mkdir -p $HOME/.config/dunst

cp -v ../dotfiles/.zshrc $HOME/
cp -v ../dotfiles/.Xresources $HOME/
cp -v ../dotfiles/.xinitrc $HOME/
cp -v ../dotfiles/.profile $HOME/
cp -v ../dotfiles/.config/i3/* $HOME/.config/i3/
# cp -v ../dotfiles/.config/i3blocks/config $HOME/.config/i3blocks/
cp -v ../dotfiles/.config/polybar/* $HOME/.config/polybar/
cp -v ../dotfiles/.config/compton/launch.sh $HOME/.config/compton/
cp -v ../dotfiles/.config/rofi/*.rasi $HOME/.config/rofi/
cp -v ../dotfiles/.config/nvim/init.vim $HOME/.config/nvim/
cp -v ../dotfiles/.config/nvim/colors/sublimemonokai.vim  $HOME/.config/nvim/colors/
cp -v ../dotfiles/.config/gtk-3.0/settings.ini $HOME/.config/gtk-3.0/
cp -v ../dotfiles/.gtkrc-2.0 $HOME/
cp -v ../dotfiles/.config/nitrogen/* $HOME/.config/nitrogen/
cp -v ../dotfiles/.config/dunst/dunstrc $HOME/.config/dunst/
cp -v ../dotfiles/.config/Code\ -\ OSS/User/settings.json $HOME/.config/Code\ -\ OSS/User/

cp -v ../media/wallpapers/wallpaper.jpg $HOME/Pictures/
