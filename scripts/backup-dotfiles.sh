#!/bin/sh

cp -v $HOME/.zshrc ../dotfiles/
cp -v $HOME/.Xresources ../dotfiles/
cp -v $HOME/.xinitrc ../dotfiles/
cp -v $HOME/.profile ../dotfiles/
cp -v $HOME/.config/i3/* ../dotfiles/.config/i3/
# cp -v $HOME/.config/i3blocks/config ../dotfiles/.config/i3blocks/
cp -v $HOME/.config/polybar/* ../dotfiles/.config/polybar/
cp -v $HOME/.config/compton/launch.sh ../dotfiles/.config/compton/
cp -v $HOME/.config/rofi/*.rasi ../dotfiles/.config/rofi/
cp -v $HOME/.config/nvim/init.vim ../dotfiles/.config/nvim/
cp -v $HOME/.config/nvim/colors/sublimemonokai.vim ../dotfiles/.config/nvim/colors/
cp -v $HOME/.config/gtk-3.0/settings.ini ../dotfiles/.config/gtk-3.0/
cp -v $HOME/.gtkrc-2.0 ../dotfiles/
cp -v $HOME/.config/nitrogen/* ../dotfiles/.config/nitrogen/
cp -v $HOME/.config/dunst/dunstrc ../dotfiles/.config/dunst/
cp -v $HOME/.config/Code\ -\ OSS/User/settings.json ../dotfiles/.config/Code\ -\ OSS/User/
