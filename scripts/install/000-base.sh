#!/bin/sh

yay -S terminus-font neovim mesa-demos htop

# remove the xorg video drivers we're not using
yay -R xf86-video-ati xf86-video-nouveau

