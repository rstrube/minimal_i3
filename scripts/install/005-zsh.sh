#!/bin/sh

# setup oh-my-zsh
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
cp -v ../../dotfiles/.zshrc $HOME/
