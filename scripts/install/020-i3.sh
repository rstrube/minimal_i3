#!/bin/sh

yay -S i3-gaps polybar i3exit dunst rxvt-unicode lightdm lightdm-slick-greeter xdg-utils xdg-user-dirs xorg-xbacklight xorg-xprop xclip sysstat compton rofi nitrogen arandr udisks2 udiskie ttf-ubuntu-font-family nerd-fonts-dejavu-complete ttf-ms-fonts polkit-qt5 polkit-gnome gksu-polkit lxappearance pcmanfm gvfs mousepad firefox xarchiver zip p7zip gcolor2 gpick viewnior epdfview pa-applet blueman clipit pamac redshift

xdg-user-dirs-update

sudo systemctl enable lightdm
sudo systemctl enable bluetooth
sudo cp -v ../../etc/lightdm/lightdm.conf /etc/lightdm/
sudo cp -v ../../etc/polkit-1/rules.d/*.rules /etc/polkit-1/rules.d/
